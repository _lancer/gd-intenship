package generator;

import com.gd.domain.*;
import com.gd.repositories.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.context.event.EventListener;

import java.util.ArrayList;
import java.util.List;

@SpringBootApplication
public class DemoDataGenerator extends SpringBootServletInitializer {

    private final static int DB_USER_SIZE = 100;
    private final static int DB_TOPIC_ROOM_SIZE = 10;
    private final static int DB_MAX_ROOM_MESSAGE_SIZE = 50;
    private final static int DB_MAX_FRIENDS_PER_USER = DB_USER_SIZE / 10;
    private final static int DB_BLOGS_PER_USER = 5;
    private final List<User> users = new ArrayList<>(DB_USER_SIZE);
    private final List<TopicRoom> rooms = new ArrayList<>(DB_TOPIC_ROOM_SIZE);

    private final UserRepository userRepository;

    private final UserRoleRepository roleRepository;

    private final RoomRepository roomRepository;

    private final RoomMessageRepository messageRepository;

    private final FriendshipRepository friendshipRepository;

    private final BlogRepository blogRepository;

    @Autowired
    public DemoDataGenerator(UserRepository userRepository, UserRoleRepository roleRepository,
                             RoomRepository roomRepository, RoomMessageRepository messageRepository,
                             FriendshipRepository friendshipRepository, BlogRepository blogRepository) {
        this.userRepository = userRepository;
        this.roleRepository = roleRepository;
        this.roomRepository = roomRepository;
        this.messageRepository = messageRepository;
        this.friendshipRepository = friendshipRepository;
        this.blogRepository = blogRepository;
    }

    public static void main(String[] args) {
        SpringApplication.run(DemoDataGenerator.class, args);
    }

    @EventListener
    public void appReady(ApplicationReadyEvent event) {
        addUsers();
        addFriends();
        addRooms();
        addMessages();
        addBlogs();
    }

    private void addFriends() {

        for (int i = 0; i < DB_USER_SIZE; i++) {
            for (int j = 1; j <= i % DB_MAX_FRIENDS_PER_USER; j++) {
                Friendship fr = new Friendship();
                fr.setUser(users.get(i));
                fr.setFriend(users.get((i * j * 31) % DB_USER_SIZE));
                friendshipRepository.save(fr);
            }
        }
    }

    private void addBlogs() {
        for (User us : users) {
            for (int i = 0; i < DB_BLOGS_PER_USER; i++) {
                Blog b = new Blog();
                b.setUser(us);
                b.setTitle(i + "yyyyyyy");
                b.setContent(i + "asd");
                blogRepository.save(b);
            }
        }
    }

    private void addUsers() {
        for (int i = 0; i < DB_USER_SIZE; i++) {
            User us = new User();
            roleRepository.findByName("REGULAR").ifPresent(us::addRole);
            us.setName("user" + i);
            us.setPassword("password");
            us.setEmail(i + "email@gmail.com");
            us.setLastname("Kowalsky");
            users.add(userRepository.save(us));
        }
    }

    private void addRooms() {
        for (int i = 0; i < DB_TOPIC_ROOM_SIZE; i++) {
            TopicRoom tr = new TopicRoom();
            tr.setRoomName("room" + i);
            for (int j = 1; j < (i % 4) + 1; j++) {
                tr.addRoomUser(users.get((i * j * 97) % DB_USER_SIZE));
            }
            rooms.add(roomRepository.save(tr));
        }
    }

    private void addMessages() {
        for (TopicRoom tr : rooms) {
            List<User> users = new ArrayList<>(tr.getRoomUsers());
            if (users.size() > 0) {
                for (int i = 0; i < DB_MAX_ROOM_MESSAGE_SIZE; i++) {
                    RoomMessage message = new RoomMessage();
                    User us = users.get((i * 31) % users.size());
                    message.setUser(us);
                    message.setContent(tr.getRoomName() + us.getName() + "::" + i);
                    message.setRoom(tr);
                    tr.addMessage(message);
                }
            }
            roomRepository.save(tr);
        }
    }
}
