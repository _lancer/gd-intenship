# GD-Internship-Project

### Project installation

- We are using Java 8 OpenJDK in project development

- You can import project into IntelliJ IDEA by selecting File -> New -> Project From existing Sources -> 
Import project from external model -> Maven

- Project consists for several micro services divided into modules,
forum-app module depends on PostgreSQL database, you can set database 
information in forumapp/src/main/resources/application.properties

- The database can be run in docker image, for this you will need Docker installed 
and ready in your environment, then run docker-compose up --build in project root,
you can change ports, volumes and database name/credentials in docker-compose.yml
   - The project uses flyway for database version control, 
   you can get it from https://flywaydb.org/documentation/commandline/ . 
   The database migration commands can be found in resources/db.migration package inside forumapp module, 
   you can see full flyway documentation under https://flywaydb.org/documentation/
   
- Logs are stored in /logs file, hibernate-search indexes in /generator/index/default, you can change 
these in application.properties

- You can run application by running mvn clean install from IntelliJ right sidebar and manually deploying created war file on server or by configuring 
Spring Boot Application configuration with main class in com.gd.Application and running it with IntelliJ