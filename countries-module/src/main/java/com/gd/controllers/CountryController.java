package com.gd.controllers;

import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class CountryController {

    @GetMapping("/countries")
    @Cacheable("countries")
    public String getCountries() throws InterruptedException {
        Thread.sleep(3000);
        return "countries";
    }

}