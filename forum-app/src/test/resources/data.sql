DROP TABLE IF EXISTS users;

CREATE TABLE users
(
    id       INT AUTO_INCREMENT PRIMARY KEY,
    name     VARCHAR(250) NOT NULL,
    lastname     VARCHAR(250) NOT NULL,
    password VARCHAR(250) NOT NULL,
    email    varchar(255) not NULL

);

DROP TABLE IF EXISTS roles;

create table roles
(
    role varchar(255)
);

DROP TABLE IF EXISTS topic_rooms;

create table topic_rooms
(
    id   INT AUTO_INCREMENT PRIMARY KEY,
    name varchar(255)
);
