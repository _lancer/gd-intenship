package com.gd.service;

import com.gd.repositories.UserRepository;
import com.gd.web.dto.UserRegistrationDto;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

@ExtendWith(SpringExtension.class)
@SpringBootTest
class UserServiceImplIntegrationTest {

    @Autowired
    UserServiceImpl userService;

    @Autowired
    UserRepository userRepository;

    @Autowired
    private BCryptPasswordEncoder passwordEncoder;


    @Test
    void loadUserByUsernameLoadsUserTest() {
        String userMail = "mail@mail.com";
        String userPassword = "pass";

        UserRegistrationDto user = new UserRegistrationDto();
        user.setEmail(userMail);
        user.setConfirmEmail(userMail);
        user.setName(userMail);
        user.setPassword(userPassword);
        user.setConfirmPassword(userPassword);
        user.setLastname(userMail);

        userService.save(user);

        UserDetails usDetails;
        usDetails = userService.loadUserByUsername(userMail);
        assertEquals(userMail, usDetails.getUsername());
        assertTrue(passwordEncoder.matches(userPassword, usDetails.getPassword()));
    }

}