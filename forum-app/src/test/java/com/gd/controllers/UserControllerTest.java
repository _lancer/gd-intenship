package com.gd.controllers;

import com.gd.domain.User;
import com.gd.repositories.UserRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.Optional;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class UserControllerTest {

    @InjectMocks
    private UserController userController;
    @Mock
    private UserRepository userRepository;

    @BeforeEach
    public void init() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testGetUserById() {
        User u = new User();
        u.setId(1);
        u.setName("Olek");
        when(userRepository.findById(1)).thenReturn(Optional.of(u));

        Optional<User> user = userController.getUserById("1");

        verify(userRepository).findById(1);

        if(user.isPresent()) {
            assertThat(user.get().getId(), is(1));
            assertThat(user.get().getName(), is("Olek"));
        }

    }
}