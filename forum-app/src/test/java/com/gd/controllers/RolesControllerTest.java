package com.gd.controllers;

import com.gd.domain.User;
import com.gd.domain.UserRole;
import com.gd.repositories.UserRepository;
import com.gd.repositories.UserRoleRepository;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;

import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

class RolesControllerTest {

    @Mock
    private static UserRepository userRepository = mock(UserRepository.class);
    private static UserRoleRepository roleRepository = mock(UserRoleRepository.class);

    private static RolesController rolesController = new RolesController(userRepository, roleRepository);

    private static int user1Id = 0;
    private static int user2Id = 1;
    private static int user3Id = 2;
    private static int wrongId = -1;
    private static User us1 = new User("Us1");
    private static User us2 = new User("Us2");
    private static User us3 = new User("Us3");
    private static UserRole ur1 = new UserRole(UserRole.RoleEnum.ADMIN);
    private static UserRole ur2 = new UserRole(UserRole.RoleEnum.REGULAR);
    private static HashSet<UserRole> us1RolesSet = new HashSet<>(Arrays.asList(ur1, ur2));
    private static HashSet<UserRole> us2RolesSet = new HashSet<>(Collections.singletonList(ur2));
    private static HashSet<UserRole> us3RolesSet = new HashSet<>();

    @BeforeAll
    public static void beforeAll() {
        when(roleRepository.findByName(UserRole.RoleEnum.REGULAR.toString()))
                .thenReturn(Optional.of(ur2));
        when(roleRepository.findByName(UserRole.RoleEnum.ADMIN.toString()))
                .thenReturn(Optional.of(ur1));
        when(userRepository.findById(user1Id)).thenReturn(Optional.of(us1));
        when(userRepository.findById(user2Id)).thenReturn(Optional.of(us2));
        when(userRepository.findById(user3Id)).thenReturn(Optional.of(us3));
    }

    @BeforeEach
    public void beforeEach() {
        us1.setRoles(us1RolesSet);
        us2.setRoles(us2RolesSet);
        us3.setRoles(us3RolesSet);
    }

    @Test
    void getUserRolesCorrectUserTest() {

        assertAll(
                () -> assertTrue(rolesController.getUserRoles(user1Id).containsAll(us1RolesSet)),
                () -> assertTrue(rolesController.getUserRoles(user2Id).containsAll(us2RolesSet)),
                () -> assertTrue(rolesController.getUserRoles(user3Id).containsAll(us3RolesSet))
        );
    }

    @Test
    void getUserRolesWrongUserTest() {
        assertThrows(Exception.class, () -> rolesController.getUserRoles(wrongId));
    }

    @Test
    void setUserRoles() {
        rolesController.setUserRoles(user1Id, Collections.singletonList(UserRole.RoleEnum.REGULAR.toString()));
        rolesController.setUserRoles(user2Id, Arrays.asList(UserRole.RoleEnum.ADMIN.toString(),
                UserRole.RoleEnum.REGULAR.toString()));
        rolesController.setUserRoles(user3Id, Collections.emptyList());

        assertAll(
                () -> assertTrue(rolesController.getUserRoles(user1Id).containsAll(us2RolesSet)),
                () -> assertTrue(rolesController.getUserRoles(user2Id).containsAll(us1RolesSet)),
                () -> assertTrue(rolesController.getUserRoles(user3Id).containsAll(us3RolesSet))
        );
    }

    @Test
    void removeRoles() {
        rolesController.removeRoles(user1Id, Collections.singletonList(UserRole.RoleEnum.ADMIN.toString()));
        rolesController.removeRoles(user2Id, Arrays.asList(UserRole.RoleEnum.ADMIN.toString(),
                UserRole.RoleEnum.REGULAR.toString()));
        rolesController.removeRoles(user3Id, Collections.emptyList());

        assertAll(
                () -> assertTrue(rolesController.getUserRoles(user1Id).containsAll(us2RolesSet)),
                () -> assertTrue(rolesController.getUserRoles(user2Id).containsAll(us3RolesSet)),
                () -> assertTrue(rolesController.getUserRoles(user3Id).containsAll(us3RolesSet))
        );
    }
}