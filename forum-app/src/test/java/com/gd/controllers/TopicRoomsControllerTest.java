package com.gd.controllers;

import com.gd.domain.RoomMessage;
import com.gd.domain.TopicRoom;
import com.gd.domain.User;
import com.gd.repositories.RoomMessageRepository;
import com.gd.repositories.RoomRepository;
import com.gd.repositories.UserRepository;
import org.junit.jupiter.api.Test;
import org.mockito.stubbing.Answer;

import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

class TopicRoomsControllerTest {

    private final int user1Id = 0;
    private final int user2Id = 1;
    private final int user3Id = 2;
    private final long room1Id = 0;
    private final long message1Id = 0;
    private final long message2Id = 1;
    private final User us1 = new User("Us1");
    private final User us2 = new User("Us2");
    private final User us3 = new User("Us3");
    private final TopicRoom tr1 = new TopicRoom();
    private final String tr1Name = "topic room name";
    private final String tr2Name = "topic room name2";
    private final String messageContent = "some message";
    private final String messageContent2 = "some message 2";
    private final String messageContent3 = "some message 3";
    private final RoomMessage message1 = new RoomMessage();
    private final RoomMessage message2 = new RoomMessage();

    private UserRepository userRepository = mock(UserRepository.class);
    private RoomMessageRepository messageRepository = mock(RoomMessageRepository.class);
    private RoomRepository roomRepository = mock(RoomRepository.class);
    private TopicRoomsController roomsController =
            new TopicRoomsController(userRepository, roomRepository, messageRepository);


    @Test
    void getAllRoomsForUserTest() {
        us1.setId(user1Id);
        us1.setRooms(new HashSet<>(Collections.singletonList(tr1)));
        when(userRepository.findById(user1Id)).thenReturn(Optional.of(us1));
        assertThat(roomsController.getAllRoomsForUser(0).getBody()).contains(tr1);
    }

    @Test
    void addRoomForUserTest() {
        us3.setId(user3Id);

        when(userRepository.findById(user3Id)).thenReturn(Optional.of(us3));
        when(userRepository.save(any())).thenAnswer((Answer<User>) invocation -> {
            Object[] args = invocation.getArguments();
            return (User) args[0];
        });
        TopicRoom tr = new TopicRoom();
        tr.setRoomName(tr2Name);
        assertThat(roomsController.addRoomForUser(user3Id, tr2Name).getBody().getRooms()).contains(tr);
    }

    @Test
    void postMessageToRoomTest() {
        when(userRepository.findById(user1Id)).thenReturn(Optional.of(us1));
        when(roomRepository.findById(room1Id)).thenReturn(Optional.of(tr1));
        us1.setId(user1Id);
        tr1.setId(room1Id);
        tr1.setRoomName(tr1Name);
        tr1.setRoomUsers(new HashSet<>(Arrays.asList(us1, us2)));

        when(messageRepository.save(any())).thenAnswer((Answer<RoomMessage>) invocation -> {
            Object[] args = invocation.getArguments();
            return (RoomMessage) args[0];
        });
        assertEquals(messageContent3, roomsController
                .postMessageToRoom(room1Id, messageContent3, user1Id).getBody()
                .getContent());

    }

}