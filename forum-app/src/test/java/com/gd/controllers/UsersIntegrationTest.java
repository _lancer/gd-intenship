package com.gd.controllers;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.gd.domain.User;
import com.gd.domain.UserRole;
import com.gd.exception.ResourceNotFoundException;
import com.gd.repositories.UserRepository;
import com.gd.repositories.UserRoleRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Optional;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith(SpringExtension.class)
@SpringBootTest
@AutoConfigureMockMvc
@Transactional
public class UsersIntegrationTest {

    private final int NUMOFTIMES = 3;
    private static int counter = 0;
    private final String[] NAMES = {"Maciek", "Ania", "Aleksander"};

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ObjectMapper objectMapper;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private UserRoleRepository userRoleRepository;

    private HashSet<UserRole> rolesSet = new HashSet<>(Arrays.asList(new UserRole(UserRole.RoleEnum.ADMIN), new UserRole(UserRole.RoleEnum.REGULAR)));

    @BeforeEach
    void initDB() {
        userRepository.deleteAll();

        for (int i = 0; i < NUMOFTIMES; i++, counter++) {
            User user = new User();
            user.setName(NAMES[i]);
            user.setPassword("password");
            user.setRoles(rolesSet);
            user.setLastname("lastname");
            user.setEmail("us@email.com");

            userRepository.save(user);
        }
    }

    @Test
    void addRoles() {
        Iterable<UserRole> userRoles = userRoleRepository.findAll();

        User us = userRepository.findById(counter)
                .orElseThrow(() -> new ResourceNotFoundException("User not found"));
        us.addRole(new UserRole(UserRole.RoleEnum.ADMIN));
        userRepository.save(us);

        assertEquals(userRoleRepository.findAll(), userRoles);
    }

    @Test
    @WithMockUser
    void testCreateUser() throws Exception {
        userRepository.deleteAll();

        for (int i = 0; i < NUMOFTIMES; i++, counter++) {

            mockMvc.perform(post("/users")
                    .contentType("application/json")
                    .param("name", NAMES[i])
                    .param("password", "password")
                    .param("lastname", "lastname")
                    .param("email", i + "us@email.com")
                    .param("roles", "[]"))
                    .andExpect(status().isOk());

            Optional<User> returnedUser = userRepository.findById(counter + 1);

            if (returnedUser.isPresent()) {
                assertThat(returnedUser.get().getName(), is(NAMES[i]));
            }
        }
    }

    @Test
    @WithMockUser
    void testUpdateUser() throws Exception {
        for (int i = 0, j = counter - NUMOFTIMES + 1; i < NUMOFTIMES; i++, j++) {
            User user = new User();
            user.setName(NAMES[i]);
            user.setPassword("password");

            mockMvc.perform(put("/users/{id}", (j))
                    .contentType("application/json")
                    .content(objectMapper.writeValueAsString(user)))
                    .andExpect(status().isOk());

            Optional<User> returnedUser = userRepository.findById(counter + 1);

            if (returnedUser.isPresent()) {
                assertThat(returnedUser.get().getName(), is(NAMES[i]));
            }
        }
    }

    @Test
    @WithMockUser
    void testDeleteUser() throws Exception {
        for (int i = 0, j = counter - NUMOFTIMES + 1; i < NUMOFTIMES; i++, j++) {
            mockMvc.perform(delete("/users/{user_id}", j))
                    .andExpect(status().isOk());

            Optional<User> returnedUser = userRepository.findById(j);

            assertThat(returnedUser, is(Optional.empty()));
        }
    }
}
