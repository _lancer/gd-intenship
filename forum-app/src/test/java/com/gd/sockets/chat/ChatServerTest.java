package com.gd.sockets.chat;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;

class ChatServerTest {

    private static final int port = 59001;
    private static final ChatServer server = new ChatServer(port);
    private final String host = "localhost";
    private final String username1 = "us1";
    private final String username2 = "us2";

    private final String message = "message";

    @BeforeAll
    public static void beforeAll() throws IOException {
        Thread thread = new Thread(() -> {
            try {
                server.run();
            } catch (IOException e) {
                e.printStackTrace();
            }
        });
        thread.start();
    }

    @Test
    public void messageReceivedTest() throws IOException {

        Socket us1Socket = new Socket(host, port);
        Socket us2Socket = new Socket(host, port);

        us1Socket.setSoTimeout(100);
        us2Socket.setSoTimeout(100);

        BufferedReader br1 = new BufferedReader(new InputStreamReader(us1Socket.getInputStream()));
        PrintWriter out1 = new PrintWriter(us1Socket.getOutputStream(), true);
        BufferedReader br2 = new BufferedReader(new InputStreamReader(us2Socket.getInputStream()));
        PrintWriter out2 = new PrintWriter(us2Socket.getOutputStream(), true);

        int counter = 0;
        out1.println(username1);
        out2.println(username2);

        try {
            while (br1.readLine() != null) {
                if (counter++ == Integer.MAX_VALUE) {
                    throw new AssertionError();
                }
            }
        } catch (java.net.SocketTimeoutException e) {
            //it's supposed to timeout
        }

        try {
            counter = 0;
            while (br2.readLine() != null) {
                if (counter++ == Integer.MAX_VALUE) {
                    throw new AssertionError();
                }
            }
        } catch (java.net.SocketTimeoutException e) {
            //it's supposed to timeout
        }

        out1.println(message);

        String receivedMessageUs1 = br1.readLine();
        String receivedMessageUs2 = br2.readLine();

        assertEquals(receivedMessageUs1, receivedMessageUs2);
        assertThat(receivedMessageUs1).contains(message);
    }
}