package com.gd.repositories;

import com.gd.domain.RoomMessage;
import com.gd.domain.TopicRoom;
import com.gd.domain.User;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import javax.transaction.Transactional;
import java.time.LocalDateTime;
import java.util.Collections;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

@ExtendWith(SpringExtension.class)
@SpringBootTest
@Transactional
class RoomMessageRepositoryTest {

    private static final int messagesNumber = 20;
    private static final int year = 2019;
    private static final int month = 7;
    private static final int day = 24;
    private static final int hour = 15;
    private static final int minute = 39;
    private static final TopicRoom room = new TopicRoom();
    private static final User user = new User();
    private static final List<RoomMessage> messages = new LinkedList<>();
    @Autowired
    private RoomMessageRepository messageRepository;
    @Autowired
    private RoomRepository roomRepository;
    @Autowired
    private UserRepository userRepository;
    private long roomId;

    @BeforeEach
    void beforeEach() {

        roomRepository.deleteAll();
        messageRepository.deleteAll();
        userRepository.deleteAll();
        messages.clear();

        room.setRoomName("room");
        user.setName("name");
        user.setEmail("email");
        user.setPassword("password");
        user.setLastname("lastname");

        User savedUser = userRepository.save(user);
        room.setRoomUsers(new HashSet<>(Collections.singletonList(savedUser)));
        TopicRoom savedRoom = roomRepository.save(room);
        roomId = savedRoom.getId();
        for (int i = 0; i < messagesNumber; i++) {
            RoomMessage message = new RoomMessage();
            message.setRoom(room);
            message.setContent("content");
            message.setUser(savedUser);
            message.setRoom(savedRoom);
            messageRepository.save(message);
            message.setCreated(LocalDateTime.of(year, month, day, hour, minute, i));
            messages.add(messageRepository.save(message));
        }
    }

    @Test
    void findNextPageTest() {
        List<RoomMessage> retrievedMessages = messageRepository.findPage(roomId, 0, 5);
        RoomMessage lastMessage = retrievedMessages.get(4);
        retrievedMessages = messageRepository.findNextPage(roomId, lastMessage.getId(), lastMessage.getCreated(), 3);
        assertEquals(retrievedMessages.size(), 3);
        assertEquals(retrievedMessages, messages.subList(5, 8));
    }

    @Test
    void findFirstPageTest() {
        List<RoomMessage> retrievedMessages = messageRepository.findPage(roomId, 0, 5);
        assertEquals(retrievedMessages.size(), 5);
        assertEquals(retrievedMessages, messages.subList(0, 5));
    }

    @Test
    void findNthPageTest() {
        List<RoomMessage> retrievedMessages = messageRepository.findPage(roomId, 6, 3);
        assertEquals(retrievedMessages.size(), 3);
        assertEquals(retrievedMessages, messages.subList(6, 9));
    }
}