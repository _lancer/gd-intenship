create table IF NOT EXISTS users
(
    id       serial       not null
        constraint users_pkey
            primary key,
    email    varchar(255) not null
        constraint uk6dotkott2kjsp8vw4d0m25fb7
            unique,
    lastname varchar(255) not null,
    name     varchar(255) not null,
    password varchar(255) not null
);

create table IF NOT EXISTS friends
(
        id        serial not null
        constraint friends_pkey
            primary key,
    friend_id integer
        constraint fkc42eihjtiryeriy8axlkpejo7
            references users,
    user_id   integer
        constraint fklh21lfp7th1y1tn9g63ihkda9
            references users
);

create table IF NOT EXISTS blogposts
(
    id      serial       not null
        constraint blogposts_pkey
            primary key,
    content text         not null,
    title   varchar(255) not null,
    user_id integer
        constraint fkrr3mnqv3qjrk6gyga29x355vi
            references users
);

create table IF NOT EXISTS roles
(
        role varchar(255) not null
        constraint roles_pkey
            primary key
);

create table IF NOT EXISTS topic_rooms
(
    id   bigserial    not null
        constraint topic_rooms_pkey
            primary key,
    name varchar(255) not null
        constraint uk_2ygtix7au83v8icp3f3i70ds0
            unique
);

create table IF NOT EXISTS room_messages
(
    id       bigserial    not null
        constraint room_messages_pkey
            primary key,
    content  varchar(255) not null,
    created  timestamp,
    modified timestamp,
    room_id  bigint
        constraint fkhbiv9dc54npj3hnocik85551p
            references topic_rooms,
    user_id  integer
        constraint fk8wu401wrd52il95pjojvmfmqf
            references users
);

create table IF NOT EXISTS users_roles
(
    user_id integer      not null
        constraint fk2o0jvgh89lemvvo17cbqvdxaa
            references users,
    role_id varchar(255) not null
        constraint fkj6m8fwv7oqv74fcehir1a9ffy
            references roles,
    constraint users_roles_pkey
        primary key (user_id, role_id)
);

create table IF NOT EXISTS users_rooms
(
    room_id bigint  not null
        constraint fkjloy5wce8tun49uyjc4pnyefq
            references topic_rooms,
    user_id integer not null
        constraint fkhiawjbkgx1ih2k4bo37rdk3x1
            references users,
    constraint users_rooms_pkey
        primary key (user_id, room_id)
);

