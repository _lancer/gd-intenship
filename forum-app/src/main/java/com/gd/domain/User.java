package com.gd.domain;

import org.hibernate.search.annotations.Index;
import org.hibernate.search.annotations.*;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Entity
@Indexed
@Table(name = "users", uniqueConstraints = @UniqueConstraint(columnNames = "email"))
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @NotNull
    @Size(max = 255)
    @Field(index = Index.YES, analyze = Analyze.YES, store = Store.NO)
    private String name;

    @NotNull
    @Size(max = 255)
    private String lastname;

    @NotNull
    private String password;

    @NotNull
    private String email;

    @ManyToMany(cascade = {CascadeType.PERSIST, CascadeType.MERGE,
            CascadeType.REFRESH, CascadeType.DETACH}, fetch = FetchType.LAZY)
    @JoinTable(
            name = "users_roles",
            joinColumns = {@JoinColumn(name = "user_id")},
            inverseJoinColumns = {@JoinColumn(name = "role_id")})
    private Set<UserRole> roles = new HashSet<>();

    @ManyToMany(cascade = {CascadeType.PERSIST, CascadeType.MERGE,
            CascadeType.REFRESH, CascadeType.DETACH}, fetch = FetchType.LAZY)
    @JoinTable(
            name = "users_rooms",
            joinColumns = {@JoinColumn(name = "user_id")},
            inverseJoinColumns = {@JoinColumn(name = "room_id")})
    private Set<TopicRoom> rooms = new HashSet<>();

    @OneToMany(mappedBy="user", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    private List<Blog> blogs = new ArrayList<>();

    @OneToMany(mappedBy = "user", fetch = FetchType.LAZY)
    private List<Friendship> friendships;

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public User() {}

    public User(String name) {
        this.name = name;
        this.password = "";
    }

    public User(String name, String password, String email) {
        this.name = name;
        this.password = password;
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void addRole(UserRole role) {
        this.roles.add(role);
    }

    public void addBlog(Blog blog) {
        this.blogs.add(blog);
    }


    public void removeRole(UserRole role) {
        this.roles.remove(role);
    }

    public void addAllRoles(Collection<UserRole> roles) {
        roles.forEach(this::addRole);
    }

    public void removeAllRoles(Collection<UserRole> roles) {
        roles.forEach(this::removeRole);
    }

    public Set<UserRole> getRoles() {
        return roles;
    }

    public void setRoles(Set<UserRole> roles) {
        this.roles = roles;
    }

    public User(String name, Blog... blogs) {
        this.name = name;
        this.blogs = Stream.of(blogs).collect(Collectors.toList());
        this.blogs.forEach(x -> x.setUser(this));
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Set<TopicRoom> getRooms() {
        return rooms;
    }

    public void setRooms(Set<TopicRoom> rooms) {
        this.rooms = rooms;
    }

    public void addRoom(TopicRoom room) {
        rooms.add(room);
    }
}