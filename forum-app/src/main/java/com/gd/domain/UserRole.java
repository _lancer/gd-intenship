package com.gd.domain;


import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

@Entity
@Table(name = "roles")
public class UserRole {

    @Id
    @Column(name = "role")
    @Enumerated(EnumType.STRING)
    private RoleEnum role;

    @JsonIgnore
    @ManyToMany(mappedBy = "roles", fetch = FetchType.LAZY)
    private Set<User> roleUsers = new HashSet<>();

    public UserRole() {
    }

    public UserRole(RoleEnum role) {
        this.role = role;
    }

    public RoleEnum getRole() {
        return role;
    }

    public void setRole(RoleEnum role) {
        this.role = role;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof UserRole)) return false;
        UserRole userRole = (UserRole) o;
        return getRole() == userRole.getRole();
    }

    @Override
    public int hashCode() {
        return Objects.hash(getRole());
    }

    public enum RoleEnum {
        ADMIN, REGULAR
    }
}