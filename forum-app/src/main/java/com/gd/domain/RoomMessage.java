package com.gd.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;
import java.util.Objects;

@Entity
@Table(name = "room_messages")
public class RoomMessage {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    @CreationTimestamp
    private LocalDateTime created;
    @UpdateTimestamp
    private LocalDateTime modified;
    @NotNull
    @Column(name = "content")
    private String content;

    @JsonIgnoreProperties("rooms")
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "user_id")
    private User user;

    @JsonIgnoreProperties("messages")
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "room_id")
    private TopicRoom room;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public TopicRoom getRoom() {
        return room;
    }

    public void setRoom(TopicRoom room) {
        this.room = room;
    }


    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public LocalDateTime getCreated() {
        return created;
    }

    public void setCreated(LocalDateTime created) {
        this.created = created;
    }

    public LocalDateTime getModified() {
        return modified;
    }

    public void setModified(LocalDateTime modified) {
        this.modified = modified;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof RoomMessage)) return false;
        RoomMessage that = (RoomMessage) o;
        return Objects.equals(getCreated(), that.getCreated()) &&
                Objects.equals(getContent(), that.getContent()) &&
                Objects.equals(getUser(), that.getUser()) &&
                Objects.equals(getRoom(), that.getRoom());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getCreated(), getContent(), getUser(), getRoom());
    }

    @Override
    public String toString() {
        return "Message{" +
                "created=" + created +
                ", content='" + content + '\'' +
                ", user=" + user +
                ", room=" + room +
                '}';
    }
}
