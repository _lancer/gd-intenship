package com.gd.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

@Entity
@Table(name = "topic_rooms")
public class TopicRoom {

    @OneToMany(mappedBy = "room", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    private Set<RoomMessage> messages = new HashSet<>();

    @JsonIgnore
    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(
            name = "users_rooms",
            joinColumns = {@JoinColumn(name = "room_id")},
            inverseJoinColumns = {@JoinColumn(name = "user_id")})
    private Set<User> roomUsers = new HashSet<>();

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @NotNull
    @Column(name = "name", unique = true)
    private String roomName;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Set<RoomMessage> getMessages() {
        return messages;
    }

    public void setMessages(Set<RoomMessage> messages) {
        this.messages = messages;
    }

    public Set<User> getRoomUsers() {
        return roomUsers;
    }

    public void setRoomUsers(Set<User> roomUsers) {
        this.roomUsers = roomUsers;
    }

    public String getRoomName() {
        return roomName;
    }

    public void setRoomName(String roomName) {
        this.roomName = roomName;
    }

    public void addMessage(RoomMessage rm) {
        messages.add(rm);
    }

    public void addRoomUser(User user) {
        this.roomUsers.add(user);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof TopicRoom)) return false;
        TopicRoom topicRoom = (TopicRoom) o;
        return getRoomName().equals(topicRoom.getRoomName());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getRoomName());
    }
}
