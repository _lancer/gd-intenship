package com.gd.controllers;

import com.gd.domain.User;
import com.gd.repositories.UserRepository;
import com.gd.repositories.UserRoleRepository;
import com.gd.search.UserSearchDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

import static com.gd.controllers.RolesController.stringCollectionToUserRoleSet;

@RestController
public class UserController {

    @Autowired
    private UserRepository userRepository;
    @Autowired
    private UserRoleRepository roleRepository;
    @Autowired
    private UserSearchDao searchDao;

    @GetMapping("/users")
    @Cacheable("users")
    public @ResponseBody
    Iterable<User> getAllUsers() throws InterruptedException {
        Thread.sleep(2000);
        return userRepository.findAll();
    }

    @GetMapping("/users/{id}")
    public @ResponseBody Optional<User> getUserById(@PathVariable String id){
        int userId = Integer.parseInt(id);
        return userRepository.findById(userId);
    }

    @GetMapping("/users/name/{name}")
    public @ResponseBody
    List<User> getUsersByName(@PathVariable String name) {
        return searchDao.searchUsersByName(name);
    }

    @GetMapping("/users/friendname/{name}")
    public @ResponseBody
    List<User> getUsersByFriendsNameName(@PathVariable String name) {
        return searchDao.searchUsersByName(name).stream()
                .flatMap(u -> userRepository.getUsersFriends(u.getId()).stream())
                .collect(Collectors.toList());
    }

    @PostMapping(value = "/users", consumes = MediaType.APPLICATION_JSON_VALUE)
    public @ResponseBody
    User createUserWithRoles(@RequestParam String name, @RequestParam String password, @RequestParam String email,
                             @RequestParam String lastname,
                             @RequestParam List<String> roles) {
        User user = new User(name);
        user.setPassword(password);
        user.setLastname(lastname);
        user.setEmail(email);
        user.setRoles(stringCollectionToUserRoleSet(roles, roleRepository));
        return userRepository.save(user);
    }

    @GetMapping("/users/regular/morefriends/5")
    public @ResponseBody
    List<User> getAllRegularUsersHavingMoreThenFiveFriends() {
        return userRepository.getAllRegularUsersHavingMoreThenFiveFriends();
    }

    @PutMapping("/users/{user_id}")
    public @ResponseBody User updateUser(@PathVariable String user_id, @RequestBody Map<String, Object> body){
        int userId = Integer.parseInt(user_id);

        Optional<User> user = userRepository.findById(userId);

        if(user.isPresent()) {
            user.ifPresent(value -> value.setName((String) body.get("name")));
            return userRepository.save(user.get());
        }

        return null;
    }

    @DeleteMapping("/users/{user_id}")
    public @ResponseBody ResponseEntity<User> deleteUser(@PathVariable String user_id){
        int userId = Integer.parseInt(user_id);
        Optional<User> user = userRepository.findById(userId);

        if(user.isPresent()) {
            userRepository.deleteById(userId);
            return ResponseEntity.status(HttpStatus.OK).body(user.get());
        }

        return ResponseEntity.status(HttpStatus.NOT_FOUND).body(null);
    }
}