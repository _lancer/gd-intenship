package com.gd.controllers;

import com.gd.domain.Friendship;
import com.gd.domain.User;
import com.gd.repositories.FriendshipRepository;
import com.gd.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.transaction.Transactional;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;

@Transactional
@RestController
public class FriendshipController {
    @Autowired
    private FriendshipRepository friendshipRepository;
    @Autowired
    private UserRepository userRepository;

    @GetMapping("/users/{user_id}/friends")
    public @ResponseBody
    Set<User> getAllFriends(@PathVariable String user_id){
        int userId = Integer.parseInt(user_id);
        List<Friendship> friendships = friendshipRepository.findAllByUserId(userId);

        Set<User> friends = new HashSet<>();

        for(Friendship friendship: friendships) {
            friends.add(friendship.getFriend());
        }
        return friends;
    }

    @PostMapping("/users/{user_id}/friends/{friend_id}")
    public @ResponseBody Friendship addFriendship(@PathVariable String user_id, @PathVariable String friend_id){
        int userId = Integer.parseInt(user_id);
        int friendId = Integer.parseInt(friend_id);

        Optional<User> user = userRepository.findById(userId);
        Optional<User> friend = userRepository.findById(friendId);

        if(user.isPresent() && friend.isPresent()) {
            Friendship friendship = new Friendship();
            friendship.setFriend(friend.get());
            friendship.setUser(user.get());

            return friendshipRepository.save(friendship);
        }
        return null;
    }

    @DeleteMapping("/users/{user_id}/friends/{friend_id}")
    public @ResponseBody boolean deleteFriend(@PathVariable String user_id, @PathVariable String friend_id){
        int userId = Integer.parseInt(user_id);
        int friendId = Integer.parseInt(friend_id);

        Optional<User> user = userRepository.findById(userId);
        Optional<User> friend = userRepository.findById(friendId);

        if(user.isPresent() && friend.isPresent()) {
            friendshipRepository.deleteFriendshipByUserIdAndFriendId(userId, friendId);
            friendshipRepository.deleteFriendshipByUserIdAndFriendId(friendId, userId);
            return true;
        }
        return false;
    }
}
