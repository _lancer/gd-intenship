package com.gd.controllers;

import com.gd.domain.RoomMessage;
import com.gd.domain.TopicRoom;
import com.gd.domain.User;
import com.gd.exception.ResourceNotFoundException;
import com.gd.repositories.RoomMessageRepository;
import com.gd.repositories.RoomRepository;
import com.gd.repositories.UserRepository;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Set;

@RestController
public class TopicRoomsController {

    private final static String USER_NOT_FOUND_MESSAGE = "Could not find user for given id";
    private final static String ROOM_NOT_FOUND_MESSAGE = "Could not find room for given id";
    private final static String MESSAGE_NOT_FOUND_MESSAGE = "Could not find message for given id";

    private final UserRepository userRepository;
    private final RoomRepository roomRepository;
    private final RoomMessageRepository messageRepository;

    public TopicRoomsController(UserRepository userRepository, RoomRepository roomRepository,
                                RoomMessageRepository messageRepository) {
        this.roomRepository = roomRepository;
        this.userRepository = userRepository;
        this.messageRepository = messageRepository;
    }

    @GetMapping("/rooms/more-messages-than-5")
    public @ResponseBody
    ResponseEntity<List<TopicRoom>> getRoomsWithMoreMessagesThan5(@RequestParam(name = "name") String name) {
        List<TopicRoom> roomList = roomRepository.findAllRoomsWithGivenNameHavingMoreMessagesThan5(name);

        return new ResponseEntity<>(roomList, HttpStatus.CREATED);
    }

    @GetMapping("/rooms/user/{user_id}")
    public @ResponseBody
    ResponseEntity<Set<TopicRoom>> getAllRoomsForUser(@PathVariable(value = "user_id") int userId) {
        return new ResponseEntity<>(userRepository.findById(userId)
                .orElseThrow(() -> new ResourceNotFoundException(USER_NOT_FOUND_MESSAGE))
                .getRooms(), HttpStatus.OK);
    }

    @PostMapping("/rooms/user/{user_id}")
    public @ResponseBody
    ResponseEntity<User> addRoomForUser(@PathVariable(value = "user_id") int userId,
                                        @RequestParam(value = "name") String roomName) {
        User us = userRepository.findById(userId)
                .orElseThrow(() -> new ResourceNotFoundException(USER_NOT_FOUND_MESSAGE));
        TopicRoom tr = new TopicRoom();
        tr.setRoomName(roomName);
        tr.addRoomUser(us);
        us.addRoom(tr);
        return new ResponseEntity<>(userRepository.save(us), HttpStatus.CREATED);
    }

    @PostMapping("/rooms/{room_id}/user/{user_id}")
    public @ResponseBody
    ResponseEntity<TopicRoom> addUserToRoom(@PathVariable(value = "room_id") long roomId,
                                            @PathVariable(value = "user_id") int userId) {
        TopicRoom tr = roomRepository.findById(roomId)
                .orElseThrow(() -> new ResourceNotFoundException(ROOM_NOT_FOUND_MESSAGE));
        User us = userRepository.findById(userId)
                .orElseThrow(() -> new ResourceNotFoundException(USER_NOT_FOUND_MESSAGE));
        tr.addRoomUser(us);

        return new ResponseEntity<>(roomRepository.save(tr), HttpStatus.CREATED);
    }

    @PostMapping("/rooms/{room_id}/message")
    public @ResponseBody
    ResponseEntity<RoomMessage> postMessageToRoom(@PathVariable(value = "room_id") long roomId,
                                                  @RequestParam(value = "message") String message,
                                                  @RequestParam(value = "user_id") int userId) {
        TopicRoom tr = roomRepository.findById(roomId)
                .orElseThrow(() -> new ResourceNotFoundException(ROOM_NOT_FOUND_MESSAGE));
        User us = userRepository.findById(userId)
                .orElseThrow(() -> new ResourceNotFoundException(USER_NOT_FOUND_MESSAGE));
        RoomMessage rm = new RoomMessage();
        rm.setRoom(tr);
        rm.setContent(message);
        rm.setUser(us);

        return new ResponseEntity<>(messageRepository.save(rm), HttpStatus.CREATED);
    }

    @GetMapping("/rooms/{room_id}/messages")
    public @ResponseBody
    ResponseEntity<Set<RoomMessage>> getAllMessages(@PathVariable(value = "room_id") long roomId) {
        TopicRoom tr = roomRepository.findById(roomId)
                .orElseThrow(() -> new ResourceNotFoundException(ROOM_NOT_FOUND_MESSAGE));

        return new ResponseEntity<>(tr.getMessages(), HttpStatus.OK);
    }

    @GetMapping("/rooms/{room_id}/messages/{from}/{to}")
    public @ResponseBody
    ResponseEntity<List<RoomMessage>> getPage(@PathVariable(value = "room_id") long roomId,
                                              @PathVariable(value = "from") int from,
                                              @PathVariable(value = "to") int to) {
        return new ResponseEntity<>(messageRepository.findPage(roomId, from, to), HttpStatus.OK);
    }

    @GetMapping("/rooms/{room_id}/messages/next")
    public @ResponseBody
    ResponseEntity<List<RoomMessage>> getNextPage(@PathVariable(value = "room_id") long roomId,
                                                  @RequestParam(value = "after_message_id") int messageId,
                                                  @RequestParam(value = "after_timestamp")
                                                  @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME) LocalDateTime timestamp,
                                                  @RequestParam(value = "limit") int limit
    ) {

        return new ResponseEntity<>(messageRepository.findNextPage(roomId, messageId, timestamp, limit), HttpStatus.OK);
    }

    @DeleteMapping("/rooms/message/{message_id}")
    public @ResponseBody
    ResponseEntity<Void> deleteMessage(@PathVariable(value = "message_id") long messageId) {
        try {
            messageRepository.deleteById(messageId);
        } catch (EmptyResultDataAccessException ex) {
            throw new ResourceNotFoundException(MESSAGE_NOT_FOUND_MESSAGE);
        }
        return ResponseEntity.accepted().build();
    }

    @PutMapping("/rooms/message/{message_id}")
    public @ResponseBody
    ResponseEntity<RoomMessage> changeMessage(@PathVariable(value = "message_id") long messageId,
                                              @RequestParam(value = "message") String message) {
        RoomMessage rm = messageRepository.findById(messageId)
                .orElseThrow(() -> new ResourceNotFoundException(MESSAGE_NOT_FOUND_MESSAGE));
        rm.setContent(message);
        return new ResponseEntity<>(messageRepository.save(rm), HttpStatus.ACCEPTED);
    }

    @GetMapping("/rooms/{room_id}")
    public @ResponseBody
    ResponseEntity<TopicRoom> getRoomById(@PathVariable(value = "room_id") long roomId) {
        return new ResponseEntity<>(roomRepository.findById(roomId)
                .orElseThrow(() -> new ResourceNotFoundException(ROOM_NOT_FOUND_MESSAGE)),
                HttpStatus.OK);

    }

    @DeleteMapping("/rooms/{room_id}")
    public @ResponseBody
    ResponseEntity<Void> deleteRoomById(@PathVariable(value = "room_id") long roomId) {
        try {
            roomRepository.deleteById(roomId);
        } catch (EmptyResultDataAccessException ex) {
            throw new ResourceNotFoundException(ROOM_NOT_FOUND_MESSAGE);
        }
        return ResponseEntity.accepted().build();
    }
}
