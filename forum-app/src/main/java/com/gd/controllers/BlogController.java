package com.gd.controllers;

import com.gd.domain.Blog;
import com.gd.domain.User;
import com.gd.repositories.BlogRepository;
import com.gd.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;
import java.util.Optional;

@RestController
public class BlogController {

    @Autowired
    private BlogRepository blogRepository;
    @Autowired
    private UserRepository userRepository;

    @GetMapping("/users/{user_id}/blogs")
    public @ResponseBody List<Blog> getAllBlogs(@PathVariable String user_id){
        int userId = Integer.parseInt(user_id);
        Optional<User> user = userRepository.findById(userId);

        return user.map(value -> blogRepository.findAllByUser(value)).orElse(null);
    }

    @GetMapping("/users/{user_id}/blogs/{blog_id}")
    public @ResponseBody Optional<Blog> getBlogById(@PathVariable String user_id, @PathVariable String blog_id){
        int userId = Integer.parseInt(user_id);
        int blogId = Integer.parseInt(blog_id);

        Optional<Blog> blog = blogRepository.findById(blogId);

        if(blog.isPresent() && blog.get().getUserId() == userId) {
            return blog;
        }

        return Optional.empty();
    }

    @PostMapping("/users/{user_id}/blogs")
    public @ResponseBody Blog create(@PathVariable String user_id, @RequestBody Map<String, String> body){
        int userId = Integer.parseInt(user_id);

        Optional<User> user = userRepository.findById(userId);
        Blog blog = new Blog();
        blog.setTitle(body.get("title"));
        blog.setContent(body.get("content"));

        user.ifPresent(blog::setUser);

        return blogRepository.save(blog);
    }

    @PutMapping("/users/{user_id}/blogs/{blog_id}")
    public @ResponseBody Blog update(@PathVariable String user_id, @PathVariable String blog_id, @RequestBody Map<String, String> body){
        int userId = Integer.parseInt(user_id);
        int blogId = Integer.parseInt(blog_id);

        Optional<Blog> blog = blogRepository.findById(blogId);

        if(blog.isPresent() && blog.get().getUserId() == userId) {
            blog.ifPresent(value -> value.setTitle(body.get("title")));
            blog.ifPresent(value -> value.setContent(body.get("content")));

            return blogRepository.save(blog.get());
        }

        return null;
    }

    @DeleteMapping("/users/{user_id}/blogs/{blog_id}")
    public @ResponseBody boolean delete(@PathVariable String user_id, @PathVariable String blog_id){
        int userId = Integer.parseInt(user_id);
        int blogId = Integer.parseInt(blog_id);

        Optional<Blog> blog = blogRepository.findById(blogId);

        if(blog.isPresent() && blog.get().getUserId() == userId) {
            blogRepository.deleteById(blogId);
            return true;
        } else return false;
    }
}