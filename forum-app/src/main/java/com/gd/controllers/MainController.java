package com.gd.controllers;

import com.gd.domain.User;
import com.gd.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import java.security.Principal;

@Controller
public class MainController {

    @Autowired
    private UserService userService;

    @GetMapping("/")
    public String root(Model model, Principal principal) {
        User user = userService.findByEmail(principal.getName());

        String userName = user.getName();
        userName = userName.substring(0, 1).toUpperCase() + userName.substring(1);

        model.addAttribute("name", userName);
        model.addAttribute("userid", user.getId());

        return "index";
    }

    @GetMapping("/login")
    public String login(Model model) {
        return "login";
    }
}

