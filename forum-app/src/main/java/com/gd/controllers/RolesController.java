package com.gd.controllers;

import com.gd.domain.User;
import com.gd.domain.UserRole;
import com.gd.exception.ResourceNotFoundException;
import com.gd.repositories.UserRepository;
import com.gd.repositories.UserRoleRepository;
import org.springframework.web.bind.annotation.*;

import java.util.Collection;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

@RestController
public class RolesController {

    private final static String USER_NOT_FOUND_MESSAGE = "Could not find user for given id";
    private UserRepository userRepository;
    private UserRoleRepository roleRepository;

    public RolesController(UserRepository userRepository, UserRoleRepository roleRepository) {
        this.userRepository = userRepository;
        this.roleRepository = roleRepository;
    }

    static Set<UserRole> stringCollectionToUserRoleSet(Collection<String> roles,
                                                       UserRoleRepository roleRepository) {

        return roles.stream().map(roleRepository::findByName)
                .filter(Optional::isPresent).map(Optional::get)
                .collect(Collectors.toSet());
    }

    @GetMapping("/users/{user_id}/roles")
    public @ResponseBody
    Collection<UserRole> getUserRoles(@PathVariable(value = "user_id") int userId) {
        User us = userRepository.findById(userId)
                .orElseThrow(() -> new ResourceNotFoundException(USER_NOT_FOUND_MESSAGE));

        return us.getRoles();
    }

    @PostMapping("/users/{user_id}/roles")
    public @ResponseBody
    void setUserRoles(@PathVariable(value = "user_id") int userId, @RequestParam List<String> roles) {
        User us = userRepository.findById(userId)
                .orElseThrow(() -> new ResourceNotFoundException(USER_NOT_FOUND_MESSAGE));
        us.setRoles(stringCollectionToUserRoleSet(roles, roleRepository));
        userRepository.save(us);
    }

    @PutMapping("/users/{user_id}/roles")
    void addUserRoles(@PathVariable(value = "user_id") int userId, @RequestParam List<String> roles) {
        User us = userRepository.findById(userId)
                .orElseThrow(() -> new ResourceNotFoundException(USER_NOT_FOUND_MESSAGE));
        us.addAllRoles(stringCollectionToUserRoleSet(roles, roleRepository));
        userRepository.save(us);
    }

    @DeleteMapping("users/{user_id}/roles")
    public @ResponseBody
    void removeRoles(@PathVariable(value = "user_id") int userId, @RequestParam List<String> roles) {
        User us = userRepository.findById(userId)
                .orElseThrow(() -> new ResourceNotFoundException(USER_NOT_FOUND_MESSAGE));
        us.removeAllRoles(stringCollectionToUserRoleSet(roles, roleRepository));
        userRepository.save(us);
    }
}