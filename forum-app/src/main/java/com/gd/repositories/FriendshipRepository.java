package com.gd.repositories;

import com.gd.domain.Friendship;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public interface FriendshipRepository extends CrudRepository<Friendship, Integer> {
    List<Friendship> findAllByUserId(int userId);
    void deleteFriendshipByUserIdAndFriendId(int userId, int friendId);
}
