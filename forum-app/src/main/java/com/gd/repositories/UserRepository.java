package com.gd.repositories;

import com.gd.domain.User;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public interface UserRepository extends CrudRepository<User, Integer> {
    User findByEmail(String email);

    @Query(value = "SELECT u.* FROM users u\n" +
            "JOIN users_roles ur ON u.ID = ur.USER_ID\n" +
            "JOIN roles r ON ur.ROLE_ID = r.ROLE\n" +
            "JOIN friends f ON u.ID = f.USER_ID\n" +
            "GROUP BY u.ID, r.ROLE\n" +
            "HAVING r.ROLE LIKE 'REGULAR' AND\n" +
            "COUNT(f.FRIEND_ID) > 5", nativeQuery = true)
    List<User> getAllRegularUsersHavingMoreThenFiveFriends();

    @Query(value = "SELECT fu.* FROM users u\n" +
            "JOIN friends f ON u.id = f.user_id\n" +
            "JOIN users fu ON f.friend_id = fu.id\n" +
            "WHERE u.id = ?1\n", nativeQuery = true)
    List<User> getUsersFriends(int id);

}