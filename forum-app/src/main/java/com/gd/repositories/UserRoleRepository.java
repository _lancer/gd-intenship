package com.gd.repositories;

import com.gd.domain.UserRole;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import java.util.Optional;

public interface UserRoleRepository extends CrudRepository<UserRole, Integer> {

    @Query(value = "SELECT * FROM roles r " +
            "WHERE r.role like ?1", nativeQuery = true)
    Optional<UserRole> findByName(String name);

}
