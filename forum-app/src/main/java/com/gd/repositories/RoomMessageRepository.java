package com.gd.repositories;

import com.gd.domain.RoomMessage;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.util.LinkedList;

@Component
public interface RoomMessageRepository extends CrudRepository<RoomMessage, Long> {

    @Query(value = "SELECT *\n" +
            "FROM room_messages\n" +
            "WHERE room_id = ?1 AND created = ?3 AND id > ?2 OR\n" +
            "created > ?3\n" +
            "ORDER BY created, id\n" +
            "LIMIT ?4\n", nativeQuery = true)
    LinkedList<RoomMessage> findNextPage(long roomId, long messageId, LocalDateTime timestamp, int limit);

    @Query(value = "SELECT *\n" +
            "FROM room_messages\n" +
            "WHERE room_id = ?1\n" +
            "ORDER BY created\n" +
            "LIMIT ?2, ?3", nativeQuery = true)
    LinkedList<RoomMessage> findPage(long roomId, long starting, int limit);

}
