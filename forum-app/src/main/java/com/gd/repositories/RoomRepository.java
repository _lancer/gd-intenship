package com.gd.repositories;


import com.gd.domain.TopicRoom;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public interface RoomRepository extends CrudRepository<TopicRoom, Long> {
    @Query(value = "SELECT tr.* FROM topic_rooms tr\n" +
            "JOIN room_messages rm ON tr.id = rm.room_id\n" +
            "where tr.name like concat(:name, '%')\n" +
            "GROUP BY tr.id\n" +
            "HAVING COUNT(rm.room_id) > 5", nativeQuery = true)
    List<TopicRoom> findAllRoomsWithGivenNameHavingMoreMessagesThan5(@Param("name") String roomName);
}
