package com.gd.repositories;

import com.gd.domain.Blog;
import com.gd.domain.User;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public interface BlogRepository extends CrudRepository<Blog, Integer> {
    List<Blog> findAllByUser(User user);
}