package com.gd;

import com.gd.sockets.chat.ChatServer;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.cache.annotation.EnableCaching;

import java.io.IOException;


@EnableCaching
@SpringBootApplication
public class Application extends SpringBootServletInitializer {

    private static ChatServer chatServer = new ChatServer(59001);

    public static void main(String[] args) throws IOException {
        SpringApplication.run(Application.class, args);
        chatServer.run();
    }

    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
        return application.sources(Application.class);
    }
}
