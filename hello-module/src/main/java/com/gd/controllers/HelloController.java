package com.gd.controllers;

import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class HelloController {

    @GetMapping("/hello")
    @Cacheable("hello")
    public String getHello() throws InterruptedException {
        Thread.sleep(2000);

        return "hello";
    }
}
